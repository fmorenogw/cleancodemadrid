
// J Unit

public class CalculatorTest {

	private static Calculator calculator;

	@BeforeEach
	public void beforeEach() {
		this.calculator = new Calculator();
	}
    
    @Test
	public void testSum() {
		// Given
		int leftAddend = 2;
		int rightAddend = 2;

		// When
		int sum = calculator.sum(leftAddend, rightAddend);

		// Then
		Assert.assertTrue(sum.isNumber());
		Assert.assertTrue(sum > leftAddend);
		Assert.assertTrue(sum > rightAddend);
		Assert.assertEquals(sum, 4);
	}

	@Test
	public void testMinus() {
		// Given
		int minuend = 2;
		int subtrahend = 2;
		
        // When
		int difference = calculator.minus(minuend, subtrahend);

        // Then
		Assert.assertTrue(sum.isNumber());
		if ((minuend > subtrahend) and (minuend > 0) and (subtrahend > 0)){
			Assert.assertTrue(difference < minuend); // Difference should be lower than minuend

		} else {
			int absDifference = calculator.minus(
				calculator.abs(minuend),
				calculator.abs(subtrahend)
			);
			Assert.assertTrue(absDifference > minuend); // Difference should be greater than minuend
		}

		Assert.assertEquals(difference, 0);
	}

	@Test
	public void testDivide() {
		// Given
		int dividend = 6;
		int divisor = 3;

        // When
		int quotient = calculator.divide(dividend, divisor);

		// Then
		Assert.assertTrue(dividend.isNumber());
		Assert.assertTrue(divisor.isNumber());
		Assert.assertFalse(divisor == 0);
		Assert.assertEquals(quotient, 2);
	}


/*
	@Test(expected = ArithmeticException.class)
	public void testDivideWillThrowExceptionWhenDivideOnZero() {
		// Given
		int dividend = 6;
		int divisor = 0;

        // When
		int quotient = calculator.divide(dividend, divisor);

		// Then
		Assert.Fail("Exception should be thrown")
	}
*/
}

	/*
	*  [...] I think the single assert rule is a good guideline [...]
	*  [...] But I am not afraid to put more than one assert in a test. 
	*        I think the best thing we can say is that the number of
	*        asserts in a test ought to be minimized.
	*
	*/

public class Connect4Test {

	// Example with multiple asserts
	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveAnyThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(0), is(false));
        assertThat(board.isAvailableMove(1), is(false));
        assertThat(board.isAvailableMove(2), is(false));
        assertThat(board.isAvailableMove(3), is(false));
        assertThat(board.isAvailableMove(4), is(false));
        assertThat(board.isAvailableMove(5), is(false));
        assertThat(board.isAvailableMove(6), is(false));
        assertThat(board.isAvailableMove(7), is(false));
    }

	// Example with one assert per test
	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveLeftThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(0), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveUpAndLeftThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(1), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveUpThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(2), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveUpAndRightThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(3), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveRightThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(4), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveDownAndRightThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(5), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveDownThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(6), is(false));
    }

	@Test
    public void testGivenFullBoardWhenIsAvaiableMoveDownAndLeftThenFalse(){
        Board board = this.boardBuilder.rows(
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR",
                "YRYRYRY",
                "RYRYRYR"
        ).build();

        assertThat(board.isAvailableMove(7), is(false));
    }

}