## FUNCTIONS

### Small functions
- 2 to 6 lines long for each function to be transparently obvious.
- Only 1 indent => everything in blocks (if, try...) should be a function call.
- Big functions are where classes hide => extract classes from functions. (A class is a group of functions that uses a common set of variables with 1 scope of responsability)

### Do One Thing
- Functions should do one thing. They should do it well. They should bo it only.
- Extract sub functions and classes until it's not possible.

### The Stepdown Rule
- Most abstract function at the top
- Sub functions with a lower level of abstraction below it.

### Switch statements
- Use them only when necessary.
- Focus on reusability.
- Do not repeat them => use `Polymorphism` and the `Abstract Factory` pattern if you can.

### Use Descriptive names
- A long descriptive name is better than a short enigmatic name.
- Don’t be afraid to spend time choosing a name.
- Hunting for a good name may result in a favorable restructuring of the code.

### Function Arguments
- Best: 0 (niladic).
- 2nd: 1 (monadic)  => function and argument should form a verb/noun pair.
- 3rd: 2 (dyadic).
- More than two should be avoided when possible.

### Reducing Arguments
- Don’t use flag arguments in a function => split the function in two.
- Use Objects: circle(x, y, radius) => circle(point, radius).
- More args => harder to test.

### No side effects
- Function should do one thing, but it also does other hidden things => bugs!

### Command Query Separation (CQS)
- Query => returns the state on an object, no modification.
- Command => modify the state, returns nothing.

### Prefer Exceptions to Returning error codes
- Returning error from command = violation of CQS.
- Error handling is one thing and functions should do one thing.

### Don't Repeat Yourself
- Eliminate duplication to have only one source of truth and one place to change.


[Credit to Ilias](https://www.linkedin.com/pulse/summary-clean-code-chapter-3-functions-robert-c-martin-el-mhamdi)