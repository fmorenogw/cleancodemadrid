
function handleSubmit(formData) {
  const isFormDataValid = validateLoginInputs(formData)
  if (isFormDataValid) userLogin(formData)
  return
}

function validateLoginInputs({ email, password }) {
  // All necessary validation goes here
  if (!email || !password || email.length < 8 || password.length < 12) {
    showErrors(true)  // Set input errors in the UI
    return false
  }
  return true
}

function userLogin(formData) {
  const authService = new AuthService()
  new authService
    .login(formData)
    .then(res => handleLoggedUser(res.data))
    .catch(error => toastContext.showError(error.response.data.message))
}

function handleLoggedUser(user) {
  userContext.setUser(user)
  toastContext.showSuccess(toastMessages.login.success)
  navigateToLandingPage()
}

const navigateToLandingPage = () => window.history.push(paths.landingPage)