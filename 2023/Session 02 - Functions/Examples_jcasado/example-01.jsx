function userLogin(formData) {
  if (formData) {
    const {e, p} = formData

    if (!e || e.length < 8) {
      showErrors(true)
    } else if (!p || p.length < 12) {
      showErrors(true)
    } else {
      const authService = new AuthService()

      authService
        .login(formData)
        .then(res => {
          userContext.setUser(res.user)
          toastContext.showSuccess('Log in successful!')
          window.history.push('/landing')
        })
        .catch(error => toastContext.showError(error.response.data.message))
    }
  }
}