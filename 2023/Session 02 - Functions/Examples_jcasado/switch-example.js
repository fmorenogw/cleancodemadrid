const getLokiCostume = hero => {
  let loki = ''

  switch (hero) {
    case 'Iron-Man':
      loki = 'Magneto'
      break
    case 'Hulk':
      loki = 'Thanos'
      break
    case 'Thor':
      loki = 'Odin'
      break
    default:
      loki = 'Loki'
      break
  }

  return loki
}