const LOKI_COSTUMES = {
  'Iron-Man': 'Magneto',
  Hulk: 'Thanos',
  Thor: 'Odin',
  Wolverine: 'Magneto'
}

const DEFAULT_LOKI_COSTUME = 'Loki'

export const getLokiCostume = hero => LOKI_COSTUMES[hero] || DEFAULT_LOKI_COSTUME