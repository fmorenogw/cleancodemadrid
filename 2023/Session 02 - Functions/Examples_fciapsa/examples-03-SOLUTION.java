public class Thermometer {
    private Temperature temperature;
    private ThermometerScale scaleSize;
    private static final double MEDIUM_THRESHOLD = 25.0;
    private static final double LARGE_THRESHOLD = 100.0;

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public void updateScaleSize() {
        double absoluteTemp = Math.abs(this.temperature.getValue());

        if(absoluteTemp <= MEDIUM_THRESHOLD) scaleSize = ThermometerScale.SMALL;
        else if (this.temperature <= LARGE_THRESHOLD) scaleSize = ThermometerScale.MEDIUM;
        else scaleSize = ThermometerScale.LARGE;
    }

    /* public void validateTemperature(Temperature providedTemperature) {
        boolean valid = Double.compare(temperature.getValue(), providedTemperature.getValue()) == 0;
        providedTemperature.setValid(valid);
    } */

    /**
     * Change the state of the owning object:
     * providedTemperature.validateUsing(thermometer)
     *
     * Temperature class would then contain a method declaration similar to:
     *
     * public void validateUsing(Thermometer thermometer) {
     *     this.valid = Double.compare(this.value, thermometer.getTemperature().getValue()) == 0;
     * }
     */

    /* public boolean setIfHot(Temperature providedTemperature) {
        if(temperature.getValue() > LARGE_THRESHOLD) {
            temperature = providedTemperature;
            return true;
        }
        return false;
    } */

    public boolean isHot() {
        return temperature.getValue() > LARGE_THRESHOLD;
    }

    /**
     * Usage:
     * if(thermometer.isHot()) thermometer.setTemperature(temperature);
     */

    public void checkConsistency() {
        try {
            checkDataConsistency();
        }
        catch (Exception e) {
            logger.log(e.getMessage());
        }
    }

    private void checkDataConsistency() {
        checkTemperatureConsistency();
        checkScaleConsistency();
        checkThresholdsConsistency();
    }

    public enum ThermometerScale {
        SMALL,
        MEDIUM,
        LARGE;
    }

}