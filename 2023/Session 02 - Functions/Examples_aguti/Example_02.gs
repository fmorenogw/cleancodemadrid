package cleancode

class Example_02 {

/*
  PROBLEMS
  1. Args height, width and starshiptype could be grouped in a class DeathStar and passed it as an argument
  2. Function is doing more than 1 thing: shoot laser, stop laser and train new soldiers (this is also unrelated to the function = side effects).
  3. Boolean in function as an argument is also a bad practice.
      It is possible to remove that boolean arg and call 2 different functions: shoot(DeathStar) and stop (DeathStar)
 */

  var forceSide : ForceSide

  function shootOrStopDeathStarLaser(height : double, width : double, starshipType : StarshipType, isLaserShooting : boolean){
    var deathStar = new DeathStar(height, width)
    if(isLaserShooting and starshipType == TC_DEATHSTAR){
      deathStar.IsLaserShooting = false
    }
    else {
      deathStar.IsLaserShooting = true
    }
    forceSide.trainNewSoldiers()
  }

}