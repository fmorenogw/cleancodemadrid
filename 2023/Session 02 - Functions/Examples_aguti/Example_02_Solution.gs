
class DeathStar {

  /*
  SOLUTION
  1. Create a new DeathStar class
  2. Set the force side inside the constructor as we know DeathStar is DarkSide
  3. Create the functions to shoot and stop the laser doing only that thing.
  */

  private var _height : double as Height
  private var _width : double as Width
  private var _starshipType : StarshipType as StarshipType
  private var _forceSide : ForceSideType as ForceSide
  private var _isLaserShooting : boolean as IsLaserShooting

  construct() {
    _starshipType = StarshipType.TC_DEATHSTAR
    _forceSide = ForceSideType.TC_DARK
    _isLaserShooting = false
  }

  construct(height : double, width : double) {
    _height = height
    _width = width
    _starshipType = StarshipType.TC_DEATHSTAR
    _forceSide = ForceSideType.TC_DARK
    _isLaserShooting = false
  }

  function shootLaser() {
    _isLaserShooting = true
  }

  function stopLaser() {
    _isLaserShooting = false
  }

}
