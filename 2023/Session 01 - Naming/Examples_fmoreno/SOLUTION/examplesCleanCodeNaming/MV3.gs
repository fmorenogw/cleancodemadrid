package fdr.lob.imf.forms.examplesCleanCodeNaming

uses fdr.lob.imf.forms.motorcarrier.AbstractMotorCarrierForm
uses gw.xml.XMLNode

// MV3

class MV3 extends AbstractMotorCarrierForm {

  function getApplicableStates() : Set<Jurisdiction> {
    return {TC_MT}
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (MotorCarrierCoverageTypes.HasElements) {
      for (covType in MotorCarrierCoverageTypes.sortBy(\covType -> covType.FixedId)) {
        var coverageTypeNode = new XMLNode("CoverageType")
        coverageTypeNode.addChild(createTextNode("CoverageTypeCode", covType.IMFCoverageTypeType.Code))
        contentNode.addChild(coverageTypeNode)
      }
    }
  }

}