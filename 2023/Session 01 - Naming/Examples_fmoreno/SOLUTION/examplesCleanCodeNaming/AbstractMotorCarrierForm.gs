package fdr.lob.imf.forms.examplesCleanCodeNaming

uses fdr.forms.inference.generic.AbstractFDRMultipleCopiesForm
uses gw.forms.FormInferenceContext
uses gw.lob.imf.IMFConstants
uses gw.xml.XMLNode

abstract class AbstractMotorCarrierForm extends AbstractFDRMultipleCopiesForm<ScheduledItem>{

  private var _motorCarrierCoverageTypes : entity.IMFCoverageType[] as readonly MotorCarrierCoverageTypes

  abstract function getApplicableStates() : Set<Jurisdiction>

  override protected function calculateExposedStates(context : FormInferenceContext) : Set<Jurisdiction> {

    var lines : PolicyLine[]

    if (Pattern.PolicyLinePatternCode != null) {
      var i = 0
      for (line in getLine(context).AssociatedPolicyPeriod.Lines){
        lines[i] = line
        i++
      }
      var line = getLine(context)
      if (line != null and line typeis IMFLine) {
        var stateCodesList = line.getAllStateMotorCarrierStates()
        var jurisdictions : Set<Jurisdiction> = {} //Jurisdictions
        for (state in stateCodesList) {
          jurisdictions.add(Jurisdiction.get(state))
        }
        return jurisdictions.intersect(getApplicableStates())
      }
    }
    return Collections.emptySet<Jurisdiction>()
  }

  protected override function getPotentialEntities(period : PolicyPeriod) : List<ScheduledItem> {
    var mcScheduledItemsList = new ArrayList<ScheduledItem>()
    var mcScheduleslist = period.IMFLine.getAllStateMotorCarrierSchedulesAsScheduledItemType().toList()
    if (mcScheduleslist.HasElements) {
      for (mcSchedule in mcScheduleslist){
        if((mcSchedule as IMFCoverageTypeScheduleConItem)
            .TypeKeyArrayCodes
            .hasMatch(\stateCode -> getApplicableStates()*.Code.contains(stateCode)) and
            not mcScheduledItemsList
                .hasMatch(\mcScheduledItem -> mcScheduledItem.PolicyContactRole_Fdr == mcSchedule.PolicyContactRole_Fdr)){
          mcScheduledItemsList.add(mcSchedule)
        }
      }
      return mcScheduledItemsList
    } else {
      return Collections.emptyList()
    }
  }

  protected function getRelatedForms() : List<Form> {
    var formsList = new ArrayList<Form>()
    //Fill list with Related Forms
    return formsList
  }

  protected function getFormNumberWithPrefix(aForm : Form) : String {
    var formPrefixStr = "IMFInlandMarine"
    return formPrefixStr + aForm.FormNumber
  }

  protected override function createFormAssociation(form: Form): FormAssociation {
    var formsList = getRelatedForms()
    for (aForm in formsList){
      aForm.FormNumber = getFormNumberWithPrefix(aForm)
    }
    return new IMFCovTypeSchConItemFormAsc(form.Branch)
  }

  protected function createIMFStateMotorCarrierEndorsementForCargoConStatesNode_Fdr(MCSchedule : productmodel.ScheduledItemStateMotorCarrierEndorsementForCargoSchedule) : XMLNode {
    var contactStates = MCSchedule.IMFStateMotorCarrierEndorsementForCargoConStates.Codes.map(\state -> state.Code).toList().sortDescending()
    return createScheduleNode("States", "State", contactStates)
  }

}
