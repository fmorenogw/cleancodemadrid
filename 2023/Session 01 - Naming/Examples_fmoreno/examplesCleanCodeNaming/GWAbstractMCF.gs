package fdr.lob.imf.forms.examplesCleanCodeNaming

uses fdr.forms.inference.generic.AbstractFDRMultipleCopiesForm
uses gw.forms.FormInferenceContext
uses gw.lob.imf.IMFConstants
uses gw.xml.XMLNode

abstract class GWAbstractMCF extends AbstractFDRMultipleCopiesForm<ScheduledItem>{

  private var motorcarriercoveragetypes : entity.IMFCoverageType[] as readonly MotorCarrierCoverageTypes

  abstract function getApplicableStates() : Set<Jurisdiction>

  override protected function calculateExposedStates(context : FormInferenceContext) : Set<Jurisdiction> {

    var lineList : PolicyLine[]

    if (Pattern.PolicyLinePatternCode != null) {
      var l = 0
      for (line in getLine(context).AssociatedPolicyPeriod.Lines){
        lineList[l] = line
        l++
      }
      var line = getLine(context)
      if (line != null and line typeis IMFLine) {
        var Statecodeslist = line.getAllStateMotorCarrierStates()
        var j : Set<Jurisdiction> = {} //Jurisdictions
        for (state in Statecodeslist) {
          j.add(Jurisdiction.get(state))
        }
        return j.intersect(getApplicableStates())
      }
    }
    return Collections.emptySet<Jurisdiction>()
  }

  protected override function getPotentialEntities(period : PolicyPeriod) : List<ScheduledItem> {
    var mcSchdItmsLst = new ArrayList<ScheduledItem>()
    var list = period.IMFLine.getAllStateMotorCarrierSchedulesAsScheduledItemType().toList()
    if (list.HasElements) {
      for (elem in list){
        if((elem as IMFCoverageTypeScheduleConItem)
            .TypeKeyArrayCodes
            .hasMatch(\stateCode -> getApplicableStates()*.Code.contains(stateCode)) and
            not mcSchdItmsLst
                .hasMatch(\elt -> elt.PolicyContactRole_Fdr == elem.PolicyContactRole_Fdr)){
          mcSchdItmsLst.add(elem)
        }
      }
      return mcSchdItmsLst
    } else {
      return Collections.emptyList()
    }
  }

  protected function fetchRelatedFormsThatAreRelatedToTheFormsAlreadyInUse() : List<Form> {
    var formsList = new ArrayList<Form>()
    //Fill list with Related Forms
    return formsList
  }

  protected function theForm(aForm : Form) : String {
    var formPrefixStr = "IMFInlandMarine"
    return formPrefixStr + aForm.FormNumber
  }

  protected override function createFormAssociation(form: Form): FormAssociation {
    var formsList = fetchRelatedFormsThatAreRelatedToTheFormsAlreadyInUse()
    for (aForm in formsList){
      aForm.FormNumber = theForm(aForm)
    }
    return new IMFCovTypeSchConItemFormAsc(form.Branch)
  }

  protected function createIMFStateMotorCarrierEndorsementForCargoConStatesNode_Fdr(MCSchedule : productmodel.ScheduledItemStateMotorCarrierEndorsementForCargoSchedule) : XMLNode {
    var contactStates = MCSchedule.IMFStateMotorCarrierEndorsementForCargoConStates.Codes.map(\state -> state.Code).toList().sortDescending()
    return createScheduleNode("States", "State", contactStates)
  }

}
