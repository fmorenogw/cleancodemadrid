package fdr.lob.imf.forms.examplesCleanCodeNaming

uses fdr.lob.imf.forms.motorcarrier.AbstractMotorCarrierForm
uses gw.xml.XMLNode

class GWFormIMC234 extends AbstractMotorCarrierForm {

  function getApplicableStates() : Set<Jurisdiction> {
    var formJurisdictions = Jurisdiction.TF_US.getTypeKeys().toSet()
    formJurisdictions.remove(Jurisdiction.TC_MT)
    return formJurisdictions
  }

  override function addDataForComparisonOrExport(contentNode : XMLNode) {
    if (MotorCarrierCoverageTypes.HasElements) {
      for (covType in MotorCarrierCoverageTypes.sortBy(\covType -> covType.FixedId)) {
        var coverageTypeNode = new XMLNode("CoverageType")
        coverageTypeNode.addChild(createTextNode("CoverageTypeCode", covType.IMFCoverageTypeType.Code))
        contentNode.addChild(coverageTypeNode)
      }
    }
  }

}