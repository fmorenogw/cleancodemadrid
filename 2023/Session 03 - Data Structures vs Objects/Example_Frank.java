package mc;

import java.util.List;

public class MacBurger {

    //    Concrete or Abstract
    public class MenuItem {
        public double PRICE_BURGER;
        public double EXTRA_DOUBLE_BURGER;
        public double EXTRA_BBQ_SAUCE;
        public String BBQ_SAUCE_INGREDIENT;
        public boolean isChesseBurger;

        public MenuItem(double PRICE_BURGER, double EXTRA_DOUBLE_BURGER, double EXTRA_BBQ_SAUCE, String BBQ_SAUCE_INGREDIENT, boolean isChesseBurger ) {
            this.PRICE_BURGER = PRICE_BURGER;
            this.EXTRA_DOUBLE_BURGER = EXTRA_DOUBLE_BURGER;
            this.EXTRA_BBQ_SAUCE = EXTRA_BBQ_SAUCE;
            this.BBQ_SAUCE_INGREDIENT = BBQ_SAUCE_INGREDIENT;
            this.isChesseBurger = isChesseBurger;
        }
    }

    //    Concrete or Abstract
    public interface MenuItem1 {
        double getIngredients()
        double getBurgerPrice()
        void setExtraIngredient()
        void addChesse()
    }

    //    Concrete or Abstract?
    public interface MenuItem2 {
        double calculatePriceInDollars()
        double hasChesse()
    }

//    DTO?? Active Record?? Bean form?

    public class BurgerDTO {
        public String meatType;
        public String chesseType;
        private String extras;

        public Burger(String meatType, String chesseType, String extras) {
            this.meatType = meatType;
            this.chesseType = chesseType;
            this.extras = extras;
        }

        public String getMeatType() {
            return meatType;
        }

        public String getChesseType() {
            return chesseType;
        }

        public String getExtras() {
            return extras;
        }

        public String getBurgerType() {
            return meatType + chesseType + extras;
        }

        public String changeTypeOfMeat(newMeat) {
            this.meatType = newMeat;
        }
    }
}
