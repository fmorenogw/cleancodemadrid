package mc;

import java.util.List;

public class McDavids {

    private abstract class MenuItem {
        public abstract double calculatePrice();
    }

    private class Burger extends MenuItem {

        private boolean isDouble;
        private List<String> ingredients;

        private final double PRICE_BURGER = 7.50;
        private final double EXTRA_DOUBLE_BURGER = 1.50;
        private final double EXTRA_BBQ_SAUCE = 1.00;
        private final String BBQ_SAUCE_INGREDIENT = "BBQ Sauce";

        @Override
        public double calculatePrice() {
            double totalPrice = PRICE_BURGER;
            if(isDouble) totalPrice += EXTRA_DOUBLE_BURGER;
            if(containsIngredient(BBQ_SAUCE_INGREDIENT)) totalPrice += EXTRA_BBQ_SAUCE;
            return totalPrice;
        }

        private boolean containsIngredient(String ingredient) {
            return ingredients.contains(ingredient);
        }
    }

    private class Fries extends MenuItem {

        private boolean isLarge;

        private final double PRICE_FRIES = 2.50;
        private final double EXTRA_LARGE_FRIES = 1.50;

        @Override
        public double calculatePrice() {
            return PRICE_FRIES + (isLarge ? EXTRA_LARGE_FRIES : 0);
        }
    }

    public double calculateMenuPrice(Burger burger, Fries fries) {
        return burger.calculatePrice() + fries.calculatePrice();
    }

}
