package bq;

import java.util.List;

public class BurgerQueen {

    private class Burger {
        public boolean isDouble;
        public List<String> ingredients;
    }

    private class Fries {
        public boolean isLarge;
    }

    private final double PRICE_FRIES = 2.50;
    private final double EXTRA_LARGE_FRIES = 1.50;
    private final double PRICE_BURGER = 7.50;
    private final double EXTRA_DOUBLE_BURGER = 1.50;
    private final String BBQ_SAUCE_INGREDIENT = "BBQ Sauce";
    private final double EXTRA_BBQ_SAUCE = 1.00;

    public double calculateMenuPrice(Burger burger, Fries fries) {
        double totalPrice = PRICE_BURGER + PRICE_FRIES;
        if(burger.isDouble) totalPrice += EXTRA_DOUBLE_BURGER;
        if(fries.isLarge) totalPrice += EXTRA_LARGE_FRIES;
        if(containsIngredient(burger.ingredients, BBQ_SAUCE_INGREDIENT)) {
            totalPrice += EXTRA_BBQ_SAUCE;
        }
        return totalPrice;
    }

    private boolean containsIngredient(List<String> ingredients, String ingredient) {
        return ingredients.contains(ingredient);
    }

}
