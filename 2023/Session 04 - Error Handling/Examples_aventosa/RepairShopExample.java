public class RepairShopExample {

    /*
     * CAR CODE
     */

    private class Car {
        private String brand;
        private String model;
        private int year;
        private boolean isWorking;

        public Car(String brand, String model, int year, boolean isWorking) {
            this.brand = brand;
            this.model = model;
            this.year = year;
            this.isWorking = isWorking;
        }

        public String getBrand() {
            return brand;
        }

        public String getModel() {
            return model;
        }

        public int getYear() {
            return year;
        }

        public boolean isWorking() {
            return isWorking;
        }

        public void setWorking(boolean working) {
            isWorking = working;
        }

        public void sendCarToRepairShop(VehicleRepairShop repairShop) {
            RepairOutcome repairInform = repairShop.repairCar(this);

            switch (repairInform) {
                case BRAND_NOT_SUPPORTED:
                    System.out.println("This car repair shop does not work for my brand");
                    break;
                case MODEL_NOT_SUPPORTED:
                    System.out.println("This car repair shop does not fix this model anymore");
                    break;
                case OLD_VEHICLE:
                    System.out.println("This car is too old to be safe. I should acquire a newer one");
                    break;
                case NOT_BROKEN:
                    System.out.println("This car is not broken at all. Why would I send it to the repair shop?");
                case REPAIRED:
                    this.setWorking(true);
                    //goForRide()
                    break;
            }

        }

    }

    /*
     * REPAIR SHOP CODE
     */

    private interface VehicleRepairShop {
        RepairOutcome repairCar(Car car);
    }

    private enum RepairOutcome {
        REPAIRED, BRAND_NOT_SUPPORTED, MODEL_NOT_SUPPORTED, OLD_VEHICLE, NOT_BROKEN
    }

    private class SkodaVehicleRepairShop implements VehicleRepairShop {

        public RepairOutcome repairCar(Car carToFix) {

            RepairOutcome repairInform = RepairOutcome.REPAIRED;

            if (carToFix.getBrand() != "Skoda") {
                repairInform = RepairOutcome.BRAND_NOT_SUPPORTED;
            } else if (carToFix.getModel() == "Superb") {
                repairInform = RepairOutcome.MODEL_NOT_SUPPORTED;
            } else if (carToFix.getYear() < 1950) {
                repairInform = RepairOutcome.OLD_VEHICLE;
            } else if (carToFix.isWorking()) {
                repairInform = RepairOutcome.NOT_BROKEN;
            }

            return repairInform;

        }
    }

}
