package aventosaexample;

/**
  SOLUTION:
  1- Instead of using multiple error codes, replace them with exceptions and separate the logic of the
  caller from the error handling section with a try/catch block.
  2- It is recommended to create a custom exception to be thrown by the repair shop to be more precise
  3- Null check the VehicleRepairShop object in the sendCarToRepairShop method to avoid a NullPointerException
  4- (Related to Chapter 3) The goForRide() method call is not related to the description and purpose of the function. It should
  be removed or placed somewhere else.
 */


public class RepairShopSolution {

    /*
     * CAR CODE
     */

    private class Car {
        private String brand;
        private String model;
        private int year;
        private boolean isWorking;

        public Car(String brand, String model, int year, boolean isWorking) {
            this.brand = brand;
            this.model = model;
            this.year = year;
            this.isWorking = isWorking;
        }

        public String getBrand() {
            return brand;
        }

        public String getModel() {
            return model;
        }

        public int getYear() {
            return year;
        }

        public boolean isWorking() {
            return isWorking;
        }

        public void setWorking(boolean working) {
            isWorking = working;
        }

        public void sendCarToRepairShop(VehicleRepairShop repairShop) {
            try {
                if (repairShop != null) { //Null check. Keep in mind you should never pass null values!
                    this.isWorking = repairShop.repairCar(this);
                }
            } catch (RepairException e) { //Log the error message
                //_log.error(e.getMessage())
                System.out.println(e.getMessage());
            }
        }
    }

    /*
     * REPAIR SHOP CODE
     */

    private interface VehicleRepairShop {
        boolean repairCar(Car car) throws RepairException; //Changed to boolean type (from RepairOutcome)
    }


    /*
    //This enumeration is not longer needed, as we will be using exceptions
    private enum RepairOutcome {
        REPAIRED, BRAND_NOT_SUPPORTED, MODEL_NOT_SUPPORTED, OLD_VEHICLE, NOT_BROKEN
    }
     */

    private class SkodaVehicleRepairShop implements VehicleRepairShop {

      public boolean repairCar(Car carToFix) throws RepairException {

        if (carToFix.getBrand() != "Skoda") {
            throw new RepairException("We cannot fix another brand apart from Skoda, try other repair shop");
        }

        if (carToFix.getModel() == "Superb") {
            throw new RepairException("This model is obsolete and we do not fix these anymore");
        }

        if (carToFix.getYear() < 1950) {
            throw new RepairException("This car is too old to be safe. Consider acquiring a newer one");
        }

        if (carToFix.isWorking()) {
            throw new RepairException("This car is working fine. Why would you try to fix it?");
        }

        return true; //We successfully repaired the car

    }
    }
}
