package aventosaexample;

public class RepairException extends  Exception{
    public RepairException(String errorMessage) {
        super(errorMessage);
    }
}