public class main {

  private var _application : AccountPoliciesProcessing

  public function main () : void {
    this._application = new AccountPoliciesProcessing(new PolicyFactoryImpl())
  }

}

public interface PolicyFactory {
  function createPolicy() : Policy
}

public class PolicyFactoryImpl implements PolicyFactory {
  override function createPolicy() : Policy {
    return new Policy()
  }

}

public class AccountPoliciesProcessing {

  private var _factory : PolicyFactory

  construct (policyFactory : PolicyFactory) {
    this._factory = policyFactory
  }

  public function createPolicy() : Policy {
    return this._factory.createPolicy()
  }

  public function modifyPolicyNumber(policy : Policy, newPolicyNumber : int) : void {
    policy.modifyPolicyNumber(newPolicyNumber) // Implemented in Policy class
  }
}