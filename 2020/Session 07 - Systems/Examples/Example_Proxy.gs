// Proxy example
uses java.lang.reflect.InvocationHandler
uses java.lang.reflect.Method
uses java.lang.reflect.Proxy

public interface Account {
  
  public function getAccountNumber () : int
  public function setAccountNumber (newAccountNumber : int) : void
  public function getPrimaryLocation () : String
  public function setPrimaryLocation (newPrimaryLocation : String) : void
  
}

//Plain Old Java Object (POJO) implementing the abstraction
public class AccountImpl implements Account {
  private var _accountNumber : int
  private var _primaryLocation : String

  override  function getAccountNumber () : int {
    return this._accountNumber
  }
  override function setAccountNumber (newAccountNumber : int) : void {
    this._accountNumber = newAccountNumber
  }
  override function getPrimaryLocation () : String {
    return this._primaryLocation
  }
  override function setPrimaryLocation (newPrimaryLocation : String) : void {
    this._primaryLocation = newPrimaryLocation
  }
} 

public class AccountProxyHandler implements InvocationHandler {
  private var _account : Account
  
  construct (account : Account) {
    this._account = account
  }

  override function invoke(proxy : Object, method : Method, args : Object[]) : Object {
    var methodName = method.getName()
    if (methodName.equals("getAccountNumber")) {
      return _account.getAccountNumber()
    } else if (methodName.equals("setAccountNumber")) {
      _account.setAccountNumber(args[0] as int)
      return null
    } else if (methodName.equals("getPrimaryLocation")) {
      return _account.getPrimaryLocation()
    } else if (methodName.equals("setPrimaryLocation")) {
      _account.setPrimaryLocation(args[0] as String)
      return null
    } else {
      // ...
    }
  }
}

public class Application {
  public function run() : void {
    var account = (Account)
    Proxy.newProxyInstance(
        Account.Class.getClassLoader(),
        new Class[]{Account},
        new AccountProxyHandler(new AccountImpl())
    )
  }
}