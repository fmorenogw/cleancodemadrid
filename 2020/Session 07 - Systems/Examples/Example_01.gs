public class main {

  private var _application : AccountPoliciesProcessing

  public function main() : void {
    this._application = new AccountPoliciesProcessing()
  }
}

public class AccountPoliciesProcessing {

  construct () {
  }

  public function createPolicy() : Policy {
    return new Policy()
  }

  public function modifyPolicyNumber(policy : Policy, newPolicyNumber : int) : void {
    policy.modifyPolicyNumber(newPolicyNumber) // Implemented in Policy class
  }
}