function setValuesForSymbols() {
  var symbolsTypelistValues : java.util.List<typekey.CoverageSymbolType>
  symbolsTypelistValues = getSymbolsTypelistValues()
  resetSymbolDescriptionValues()
  symbolsTypelistValues.each(\symbol ->  {assignSymbolData(symbol)})
}

function resetSymbolDescriptionValues(){
  if (shouldSymbolDescriptionValuesBeReset()) {
    for (vehicleGroupDescription in this.CA7VehGroupDescriptions) {
      this.removeFromCA7VehGroupDescriptions(vehicleGroupDescription)
    }
  }
}

function shouldSymbolDescriptionValuesBeReset() : boolean{
  var covType : CoverageSymbolType
  covType = this.CA7LinePolicyType == CA7LineConstants.BUSINESS_AUTO_LINE_TYPE
      ? CoverageSymbolType.TC_SYMBOL_21 : CoverageSymbolType.TC_SYMBOL_1
    return hasCovTypeMatch(covType)
}

function hasThisMatch(covType : typekey.CoverageSymbolType) : boolean {
  return this.CA7VehGroupDescriptions.hasMatch (\vehicleGroupDescription ->
      (vehicleGroupDescription.CovSymbolTypeCode == covType)
  )
}

function assignSymbolData(symbol : typekey.CoverageSymbolType){
  var groupDescData : CA7VehGroupDes
  groupDescData = new CA7VehGroupDes(this.Branch)
  groupDescData.CovSymbolTypeCode = symbol
  if (hasSymbolFreeFormText(symbol)) {
    groupDescData.FreeFormText = setVehicleGroupSymbolReferenceDesc(symbol)
  }
  this.addToCA7VehGroupDescriptions(groupDescData)
}

function hasSymbolFreeFormText(symbol : CoverageSymbolType) : boolean {
  return (symbol == CoverageSymbolType.TC_SYMBOL_10 or
      symbol == CoverageSymbolType.TC_SYMBOL_11 or
      symbol == CoverageSymbolType.TC_SYMBOL_12 or
      symbol == CoverageSymbolType.TC_SYMBOL_13)
}