function setValuesForSymbols() : boolean {
  var symbolsTypelistValues : java.util.List<typekey.CoverageSymbolType>
  symbolsTypelistValues = getSymbolsTypelistValues()
  var shouldExistingArrayBeDeleted = false

  if (!this.CA7VehGroupDescriptions.IsEmpty) {
    if (this.CA7LinePolicyType == CA7LineConstants.BUSINESS_AUTO_LINE_TYPE) {
      for (vehicleGroupDescription in this.CA7VehGroupDescriptions) {
        if (vehicleGroupDescription.CovSymbolTypeCode == CoverageSymbolType.TC_SYMBOL_21) {
          shouldExistingArrayBeDeleted = true
        }
      }
    } else {
      for (vehicleGroupDescription in this.CA7VehGroupDescriptions) {
        if (vehicleGroupDescription.CovSymbolTypeCode == CoverageSymbolType.TC_SYMBOL_1) {
          shouldExistingArrayBeDeleted = true
        }
      }
    }
  }
  if (shouldExistingArrayBeDeleted) {
    for (vehicleGroupDescription in this.CA7VehGroupDescriptions) {
      this.removeFromCA7VehGroupDescriptions(vehicleGroupDescription)
    }
  }
  for (symbol in symbolsTypelistValues) {
    var groupDescData = new CA7VehGroupDes(this.Branch)
    groupDescData.CovSymbolTypeCode = symbol
    if (symbol == CoverageSymbolType.TC_SYMBOL_10 or
        symbol == CoverageSymbolType.TC_SYMBOL_11 or
        symbol == CoverageSymbolType.TC_SYMBOL_12 or
        symbol == CoverageSymbolType.TC_SYMBOL_13) {
      groupDescData.FreeFormText = setVehicleGroupSymbolReferenceDesc(symbol)
    }
    this.addToCA7VehGroupDescriptions(groupDescData)
  }
  return !shouldExistingArrayBeDeleted
}