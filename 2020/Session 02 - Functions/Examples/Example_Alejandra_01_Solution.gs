//Problems: 
//Arguments could be grouped in a class "Lamp"
//Function does more than one thing (turns on and off)
//Condition of the if is not necessary as we will not be changing the value if not needed
//Function has side effects


//Solution:

class Lamp {

  private var _isOn : boolean as IsOn
  private var _bulbType : LightBulbType as BulbType
  private var _lampHeight : double as LampHeight
  
  construct(){
    isOn = false;
  }
  
  construct(isOn : boolean, bulbType : LightBulbType, lampHeight : double){
  _isOn = isOn
  _bulbType = bulbType
  _lampHeight = lampHeight
  }
  
  
  /**
   * Turns this lamp on.
   */
  public void turnOn()
  {
    isOn = true;
  }
  
   /**
   * Turns this lamp off.
   */
  public void turnOff()
  {
    isOn = false;
  }