/**
 * Problems with initial solution:
 * Dependencies between locked methods, which can cause issues using them on a shared object, since the client needs to call two of them.
 * Several critical sections, dependent on each other. Causing a potential error in the last iteration.
 * Code fixed using server-based locking and adapting the client. The Iterator interface is inherently not thread-safe.
 */
class IntegerIteratorServiceLocked {
  var _nextValue = 0
  final static var MAX_ITERATIONS = 100

  //Single function that locks the server, calls all the methods and then unlocks
  function getNextOrNull() : Integer {
    using (this as IMonitorLock) {
      if (_nextValue < MAX_ITERATIONS) {
        _nextValue++
        return _nextValue
      } else {
        return null
      }
    }
  }
}

class IntegerIteratorLockedRunnable implements Runnable {
  var _iterator : IntegerIteratorServiceLocked

  construct(integerIterator : IntegerIteratorServiceLocked) {
    this._iterator = integerIterator
  }

  override function run() {
    while (true) {
      var nextValue = _iterator.getNextOrNull()
      if (nextValue == null) {
        break
      }
      print("Thread: ${Thread.currentThread().getName()}, Next: ${nextValue}")
    }
  }
}

class Main {
  public function main() : void {
    var integerIteratorService = new IntegerIteratorServiceLocked()
    var integerIteratorRunnable = new IntegerIteratorLockedRunnable(integerIteratorService)
    var thread1 = new Thread(integerIteratorRunnable, "T1")
    var thread2 = new Thread(integerIteratorRunnable, "T2")
    thread1.start()
    thread2.start()
  }
}