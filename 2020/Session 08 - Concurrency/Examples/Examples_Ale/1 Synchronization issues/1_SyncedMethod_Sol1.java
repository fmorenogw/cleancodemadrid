import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


class SynchronizedCounter {
    private int count = 0;

    // Synchronized Method
    public synchronized void increment() {
        count++;
    }

    public int getCount() {
        return count;
    }
}

public class SynchronizedMethodExample {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        SynchronizedCounter synchronizedCounter = new SynchronizedCounter();

        for(int i = 0; i < 1000; i++) {
            executorService.submit(() -> synchronizedCounter.increment());
        }

        executorService.shutdown();
        executorService.awaitTermination(60, TimeUnit.SECONDS);

        System.out.println("Final count is : " + synchronizedCounter.getCount());
    }
}

/*
It's really easy for synchronized methods to generate bugs!! 
Whenever there are static and non static synched methods mixed, both of them are locked differently.
A synchronized method synchronizes on the object instance or the class. 
A thread only executes a synchronized method after it has acquired the lock for the method's object or class.

static synchronized methods synchronize on the class object. If one thread is executing a static synchronized
method, all other threads trying to execute any static synchronized methods, in the same class, will block.

non-static synchronized methods synchronize on "this" (the instance object). If one thread is executing a 
synchronized method, all other threads trying to execute any synchronized methods, in the same class, will block.
*/
