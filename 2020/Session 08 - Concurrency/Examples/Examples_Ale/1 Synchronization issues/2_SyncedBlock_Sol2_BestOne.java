import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


class FineGrainedSynchronizedCounter {
    private int count = 0;

    public void increment() {
        // Synchronized Block
        synchronized (this) { //Here the lock is aquired
            count++;
        }
		//Lock is released
    }

    public int getCount() {
        return count;
    }
}

public class SynchronizedBlockExample {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        FineGrainedSynchronizedCounter counter = new FineGrainedSynchronizedCounter();

        for(int i = 0; i < 1000; i++) {
            executorService.submit(() -> counter.increment());
        }

        executorService.shutdown();
        executorService.awaitTermination(60, TimeUnit.SECONDS);

        System.out.println("Final count is " + counter.getCount());
    }
}

/*
It's better because:
	- It reduces the scope of lock
	- Has better performance as only the critical section is locked
	- It gives granular control over lock, avoiding issues shown by synced methods locking either on the
	current object represented by "this" or the class level lock.
	
	- It can throw a NPE! This is important because if the value within the param of the ```synchronized()```
	is null, it will take control.
	*/