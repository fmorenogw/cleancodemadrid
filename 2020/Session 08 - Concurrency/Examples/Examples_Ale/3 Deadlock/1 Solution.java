package thread;
 
public class MyTestClass {
 
    public static void main(String[] args) {
        MyTestClass test = new MyTestClass();
 
        final Pizza myPizza = test.new Pizza();
        final Burger myBurger = test.new Burger();
 
        // Thread-1
        Runnable block1 = new Runnable() {
            public void run() {
                synchronized (myBurger) {
                    // Thread-1 has Pizza but needs Burger also
                    synchronized (myPizza) {
                        System.out.println("In block 1");
                    }
                }
            }
        };
 
        // Thread-2
        Runnable block2 = new Runnable() {
            public void run() {
                synchronized (myBurger) {
                    // Thread-2 has Burger but needs Pizza also
                    synchronized (myPizza) {
                        System.out.println("In block 2");
                    }
                }
            }
        };
 
        new Thread(block1).start();
        new Thread(block2).start();
    }
 

    private class Pizza {
        private int i = 10;
 
        public int getI() {
            return i;
        }
 
        public void setI(int i) {
            this.i = i;
        }
    }
 
    private class Burger {
        private int i = 20;
 
        public int getI() {
            return i;
        }
 
        public void setI(int i) {
            this.i = i;
        }
    }
}