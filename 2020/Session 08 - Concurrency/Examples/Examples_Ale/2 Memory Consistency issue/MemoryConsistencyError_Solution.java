/*
Unfortunately the previous shown program not only provides the following output:
	Say Hello..
	Say Bye..
but it also DOES NOT TERMINATE.
The first thread is NOT AWARE of the changes done by the main thread to the sayHello var.
Lovely Memory Consistency Error.
Here's when volatile comes handy. If you mark a variable as volatile, 
the compiler won’t optimize or reorder instructions around that variable.
AND MOST IMPORTANTLY, THE VALUE WILL ALWAYS BE READ DIRECTLY FROM MEMORY!
*/

public class VolatileKeywordExample {
    private static volatile boolean sayHello = false;

    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(() -> {
           while(!sayHello) {
           }

           System.out.println("Hello World!");

           while(sayHello) {
           }

           System.out.println("Good Bye!");
        });

        thread.start();

        Thread.sleep(1000);
        System.out.println("Say Hello..");
        sayHello = true;

        Thread.sleep(1000);
        System.out.println("Say Bye..");
        sayHello = false;
    }
}