

public class MemoryConsistencyErrorExample {
	
	private static boolean sayHello = false;

	public static void main(String[] args) throws InterruptedException {

		Thread thread = new Thread(() -> {
		   while(!sayHello) {

		   }

		   System.out.println("Hello World!");

		   while(sayHello) {

		   }

		   System.out.println("Good Bye!");
		});

		thread.start();

		Thread.sleep(1000);
		System.out.println("Say Hello..");
		sayHello = true;

		Thread.sleep(1000);
		System.out.println("Say Bye..");
		sayHello = false;
	}
}

/*
Expected behavior:
Wait for one second and then print Hello World! after sayHello becomes true.
Wait for one more second and then print Good Bye! after sayHello becomes false.

Will the output be (as expected):
	Say Hello..
	Hello World!
	Say Bye..
	Good Bye!
????
*/
