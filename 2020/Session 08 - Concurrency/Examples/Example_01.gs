class IntegerIteratorService implements Iterator<Integer> {
  var _nextValue = 0
  final static var MAX_ITERATIONS = 100

  override function hasNext() : boolean {
    using (this as IMonitorLock) {
      return _nextValue < MAX_ITERATIONS
    }
  }

  override function next() : Integer {
    using (this as IMonitorLock) {
      if (_nextValue == MAX_ITERATIONS) {
        throw new IteratorPastEndException("Maximum allowed value exceeded")
      }
      _nextValue++
      return _nextValue
    }
  }

  property get NextValue() : Integer{
    using(this as IMonitorLock){
      return _nextValue
    }
  }
}

class IntegerIteratorRunnable implements Runnable{
  var _iterator : IntegerIteratorService

  construct(integerIterator : IntegerIteratorService) {
    this._iterator = integerIterator
  }

  override function run() {
    while(_iterator.hasNext()) {
      print("Thread: ${Thread.currentThread().getName()}, Next: ${_iterator.next()}")
    }
  }
}

class Main {
  public function main() : void {
    var integerIteratorService = new IntegerIteratorService()
    var integerIteratorRunnable = new IntegerIteratorRunnable(integerIteratorService)
    var thread1 = new Thread(integerIteratorRunnable, "T1")
    var thread2 = new Thread(integerIteratorRunnable, "T2")
    thread1.start()
    thread2.start()
  }
}