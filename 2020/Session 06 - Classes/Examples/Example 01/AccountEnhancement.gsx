
/**
 * Enhancements for the Account entity.
 */
enhancement AccountEnhancement: Account {

  /**
   * Creates a shallow copy of the Account . It does not copy any related entities, i.e. AccountPeriod,
   * AccountUsage.
   */
  function createCopy() : Account {
    var newAccount : Account
    if (this typeis AccountLiability) {
      newAccount = new AccountLiability()
    } else {
      newAccount = new AccountKasko()
    }
    // Copying values
    newAccount.VehicleGroup = this.VehicleGroup
    newAccount.TransferOutLicensePlate = this.TransferOutLicensePlate

    var newPriorLossHistory = this.PriorLossHistory.map(\priorLoss -> priorLoss.copy(newAccount))
    newAccount.PriorLossHistory = newPriorLossHistory

    newAccount.PreviousInsurer = this.PreviousInsurer
    newAccount.TransferOutInsurer = this.TransferOutInsurer

    return newAccount
  }

  /**
   * Finds the Account  Period where the usageYear supplied as a param intersects the effective
   * date range of the Account  Period to be returned.
   */
  @Param("usageYear", "The usage year of the period to be retrieved")
  @Returns("The AccountPeriod where the lookup date intersects the effective date range of that period")
  function getPeriodForYear(usageYear: int): AccountPeriod {
    return this.Periods.where(\period -> period.UsageYear == usageYear).first()
  }

  /**
   * Finds the latest Account  Period that matches the given date (if any). When no date is given, it searches for
   * the latest in the whole list of periods. Also, the premature period can be considered or not (default to no).
   */
  @Param("date", "The maximum date from which to search the latest Account  Period.")
  @Param("includePrematurePeriod", "Whether to include the premature period (if it exists) or not in the search.")
  @Returns("The latest Account  Period that is found.")
  function getLatestPeriodBefore(date: Date = null, includePrematurePeriod: boolean = false): AccountPeriod {
    var accountPeriods = this.Periods.where(\p -> date == null or p.BaseYear == null or p.BaseYear < date.YearOfDate)

    if (not includePrematurePeriod) {
      accountPeriods = AccountPeriods.where(\p -> not p.PrematurePeriod)
    }

    return accountPeriods.sortByDescending(\p -> p.BaseYear).first()
  }

   /**
   * Returns the current  period relative to today's date.
   */
  property get CurrentPeriod(): AccountPeriod {
    return getLatestPeriodBefore(DateUtil.currentDate())
  }

  /**
   * Returns the latest  period (not including "premanure" intervals).
   */
  property get LatestPeriod(): AccountPeriod {
    return getLatestPeriodBefore()
  }

  /**
   * Returns the latest  period including "premanure" intervals.
   */
  property get LatestPeriodIncludingPremature(): AccountPeriod {
    return getLatestPeriodBefore(null, true)
  }
  
  /**
   * Find the earliest Account  Period relative closest to the input year.
   */
  @Param("year", "The year to find the closest Account  Period")
  @Returns("Earliest Account  Period")
  function getEarliestPeriodInOrAfterYear(year: int): AccountPeriod {
    return this.Periods
      .where(\elt -> year == 0 or elt.BaseYear == null or elt.BaseYear >= year)
      .sortBy(\elt -> elt.BaseYear)
      .first()
  }

  /**
   * Checks whether this Account  can be used for the policy linked to the input Policy Period.
   */
  @Param("policyPeriod", "Policy period to check")
  @Returns("True if  can be used on the policy; otherwise, false.")
  function isAvailable(policyPeriod: PolicyPeriod): boolean {
    return isCreationOrTransferInBeforeOrEqual(policyPeriod.EditEffectiveDate) and
      isTransferOutDateAfter(policyPeriod.PeriodEnd) and
      AccountUsageChecker.Instance.isAvailable(this, policyPeriod)
  }

  function findPolicyByAccount(): Policy {
    return new PolicyDatabaseFinder().findPolicyByAccount(this)
  }

  /**
   * Checks whether this Account  can be reused for the policy linked to the input Policy Period.
   */
  @Param("policyPeriod", "Policy period to check")
  @Returns("True if  can be reused on the policy; otherwise, false.")
  function isReusable(policyPeriod: PolicyPeriod): boolean {
    return isCreationOrTransferInBeforeOrEqual(policyPeriod.EditEffectiveDate) and
      isTransferOutDateAfter(policyPeriod.PeriodEnd) and
      isActiveAccount(this) and
      AccountUsageChecker.Instance.isSimultaneousUsable(this, policyPeriod)
  }

  /**
   * Checks whether this Account  is reused in the given period defined by start date and end date.
   */
  @Param("startDate", "period start")
  @Param("endDate", "period end")
  @Returns("True if  is reused in the given period; otherwise, false.")
  function isReused(startDate : Date, endDate : Date) : boolean {
    return AccountUsageChecker.Instance.isSimultaneouslyUsed(this, startDate, endDate)
  }

  /**
   * Checks whether this Account  can be used during the date range from start date to end date. If start or
   * end date are not set (null), they are interpreted as "forever".
   */
  @Param("startDate", "Start date of the interval")
  @Param("endDate", "End date of the interval")
  @Returns("True if  can be used during the date range; otherwise, false.")
  function isAvailable(startDate: Date, endDate: Date): boolean {
    return isCreationOrTransferInBeforeOrEqual(startDate) and
      isTransferOutDateAfter(endDate) and
      AccountUsageChecker.Instance.isAvailable(this, startDate, endDate)
  }

  /**
   * Determines if the input Claim is relavant for  calculations, based on the claim's liability/KASKO indicators.
   * In case of liability claims, an incident needs to be reported to new insurers even it is not  relevant
   * (following Pflichtversicherungsgesetz).
   */
  @Param("claim", "Input claim to check")
  @Returns("True if the claim is relevant for  calculations")
  function isRelevantClaim(claim: Claim): boolean {
    return this typeis AccountLiability ? claim.LiabilityRelevant : claim.KaskoRelevant
  }

  /**
   * This function removes the Account if there are no other usages and the Account is
   * deemed newly created (i.e. was not inherited through a internal transfer process).
   * It will also remove the AccountPair if it does not have any other account  linked to it.
   */
  function removeIfNoOtherUsagesAndCreatedNew() {
    // If this Account was Transferred-In Internally then it shouldn't be deemed as a new  and a removal should be avoided.
    if (!this.New and this.TransferInInternally) {
      return
    }

    var usageCount = this.Periods*.Usages.Count

    if (usageCount == 0) {
      var relatedHistorys = this.Bundle.getBeansByRootType(History).where(\history -> (history as History).Account == this)
      relatedHistorys*.remove()
      this.remove()
    }
  }

  /**
   * Returns the SFVersionNumber from the Product Version on the most recent Policy associated with this Account.
   */
  public property get CurrentSFVersion(): Integer {
    return new PolicyDatabaseFinder().findBoundByAccount(this)
        .orderByDescending(\elt -> elt.EffectiveDate)
        .first()
        ?.ProductVersion
        ?.ConfigFinder
        .SFVersionNumber
  }



  /**
   * Returns the external SF class for the latest  period.
   */
  property get LatestExtSFClass(): SFClass {
    return this.LatestPeriod.ExtSFClass
  }

  /**
   * Returns the internal SF class for the latest  period.
   */
  property get LatestIntSFClass(): SFClass {
    return this.LatestPeriod.IntSFClass
  }

  /**
   * Returns the base year for the latest  period.
   */
  property get LatestBaseYear(): Integer {
    return this.LatestPeriod.BaseYear
  }

  /**
   * Returns the earliest  period for the account , regardless of the date.
   */
  property get EarliestPeriod(): AccountPeriod {
    return getEarliestPeriodInOrAfterYear(0)
  }

  /**
   * Determines if this  is being transferred using VWB processes.
   */
  @Returns("True if the  transfer process is Transfer In")
  property get TransferIn(): Boolean {
    return this.Pair.TransferIn
  }

  /**
   * Determines if this  is being transferred internally
   */
  @Returns("True if the  transfer process is Transfer In Internally")
  property get TransferInInternally(): Boolean {
    return this.Pair.TransferInInternally
  }

  @Returns("True if the  transfer process is Transfer Out")
    property get TransferOut(): Boolean {
      return this.Pair.TransferOut
  }

  /**
   * Determines if this  is being transferred in a phsyical §5 PflvG ertificate.
   */
  @Returns("True if the  transfer process is Transfer In Using Document")
  property get TransferInUsingDocument(): Boolean {
    return this.Pair.TransferInUsingDocument
  }

  @Returns("True if the  transfer process is Transfer Out Using Document")
  property get TransferOutUsingDocument(): Boolean {
    return this.Pair.TransferOutUsingDocument
  }

  /**
   * Returns true of this  contains a record of taking over another owner's , e.g. a son is taking over
   * his father's  (because the father does not drive anymore). The original  is marked with
   * the Taken Over status and cannot be used on policies anymore.
   */
  @Returns("True if this  is created as a result of taking over someone else's ")
  @Deprecated("Inspect  transactions for transfers instead")
  property get TakesOverOther(): boolean {
    return false
  }

  /**
   * If  is marked as not to be used anymore, returns false. Otherwise, it is true.
   */
  @Returns("False if this  should not be used anymore; otherwise, true.")
  property get Usable(): boolean {
    return StatusOK and PairOK
  }

  /**
   * Checks for any active usage in the Account  Usages table for a given date.
   */
  public function isInUseOnDate(testDate : Date) : boolean {
    return this.Periods*.Usages.firstWhere(
        \u -> u.UsedFrom.beforeOrEqual(testDate) and u.UsedUntil.after(testDate)) != null
  }

  /**
   * @return Returns true if the  status does not disqualify this  from being usable.
   */
  private property get StatusOK(): boolean {
    var statusesWhichMakeNotUsable: List<typekey.AccountStatus> = {
      TC_TAKEN_OVER,
      TC_TRANSFERRED_OUT,
      TC_TRANSFERRED_OUT_INTERNALLY
    }

    return not statusesWhichMakeNotUsable.contains(this.Status)
  }

  /**
   *  If the pair exists, it must be usable
   *  @return Returns true if the pair does not exist but if it exists, is must be ok
   */
  private property get PairOK(): boolean {
    var Pair = this.Pair
    return Pair == null ? true : Pair.Usable
  }

  /**
   * Returns the most recent expiration status applied to this account .
   */
  @Returns("Latest expiration status, or null if never expired")
  property get LatestExpirationStatus(): ExpirationStatus {
    return this.ExpirationStatuses.orderByDescending(\s -> s.Date).first().Status
  }

  /**
   * Returns true if the Account  is currently expired.
   */
  @Returns("True if the  is in the expired state")
  property get Expired(): boolean {
    return LatestExpirationStatus == TC_EXPIRED
  }

  /**
   * Returns the date of the last time the Account was set to expired.
   */
  @Returns("The date when the  was set to the expired state the last time")
  property get LatestExpirationDate(): Date {
    return getLatestByExpirationStatus(TC_EXPIRED).Date
  }

  /**
   * Returns the date of the last time the Account was reactivated.
   */
  @Returns("The date when the  was set to the reactivated state the last time")
  property get LatestReactivationDate(): Date {
    return getLatestByExpirationStatus(TC_REACTIVATED).Date
  }

  /**
   * Returns the latest risk indicator from the  periods usages.
   */
  @Returns("The last known risk indicator on the most recent  usage")
  property get LatestRiskIndicator(): VehicleRiskIndicatorReference {
    return this.Periods
      .sortByDescending(\period -> period.BaseYear)
      .map(\period -> period.LatestUsage)
        .firstWhere(\usage -> usage.RiskIndicator != null)
      .RiskIndicator
  }

  /**
   * Returns the last available Usage from all associated Periods.
   */
  property get LatestUsage(): AccountPeriodUsage {
    return this.Periods*.Usages.fastList()
        .sortBy(\usage -> usage.UsedUntil)
        .first()
  }

  /**
   * Returns the  pair.
   *
   * In the data model the foreign keys to the subtypes (Liability and Kasko) are defined on the  pair entity,
   * and the corresponding one-on-one property - on the subtypes. Since most usages would prefer to access it
   * from the supertype, the one-on-one property on the subtypes are hidden in Gosu and exposed as a set of
   * getter and a setter on the supertype. As a result, reflection is used to get and set these properties.
   *
   * @return The instance of  Pair entity.
   */
  property get Pair(): AccountPair {
    var getReflectively = \name: String -> {
      var result : AccountPair
      try {
        result = ReflectUtil.getProperty(this, name) as AccountPair
      } catch ( e : Exception) {}
      return result
    }

    switch (typeof this) {
      case AccountLiability:
        return getReflectively("PairLiability")
      case AccountKasko:
        return getReflectively("PairKasko")
      default:
        return null
    }
  }

  /**
   * Sets the value of the  pair.
   *
   * In the data model the foreign keys to the subtypes (Liability and Kasko) are defined on the  pair entity,
   * and the corresponding one-on-one property - on the subtypes. Since most usages would prefer to access it
   * from the supertype, the one-on-one property on the subtypes are hidden in Gosu and exposed as a set of
   * getter and a setter on the supertype. As a result, reflection is used to get and set these properties.
   */
  property set Pair(val: AccountPair) {
    var setReflectively = \name: String -> ReflectUtil.setProperty(this, name, val)
    switch (typeof this) {
      case AccountLiability:
        setReflectively("PairLiability")
        break
      case AccountKasko:
        setReflectively("PairKasko")
        break
    }
  }

  /**
   * Return TransferInDate retrieved from the  pair
   */
  property get TransferInDate() : Date {
    return this.Pair.TransferInDate
  }

  /**
   * Return TransferOutDate retrieved from the  pair
   */
  property get TransferOutDate() : Date {
    return this.Pair.TransferOutDate
  }

  private function isCreationOrTransferInBeforeOrEqual(startDate: Date): boolean {
    return (startDate == null or this.EarliestPeriod.BaseYear < startDate.YearOfDate)
      and (not this.TransferIn or this.TransferInDate?.beforeOrEqual(startDate))
  }

  private function isTransferOutDateAfter(endDate: Date): boolean {
    var noTransferOutDate = this.TransferOutDate == null
    var infiniteAvailabilityNeeded = endDate == null
    return noTransferOutDate or (not infiniteAvailabilityNeeded and this.TransferOutDate?.after(endDate))
  }

  private function isActiveAccount(Account: Account): boolean {
    return Account.Status == AccountStatus.TC_ACTIVE
  }

  private function getLatestByExpirationStatus(type: ExpirationStatus): AccountExpirationStatus {
    return this.ExpirationStatuses.where(\s -> s.Status == type).orderByDescending(\s -> s.Date).first()
  }
}