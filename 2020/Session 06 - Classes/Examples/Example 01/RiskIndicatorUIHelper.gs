
class RiskIndicatorUIHelper {

  var _riFactorsFillable : RIFactorsFillable

  construct(riFactorsFillable : RIFactorsFillable) {
    _riFactorsFillable = riFactorsFillable
  }

  function onVehicleTypeChange(asOfDate: Date) : List<VehicleRiskIndicatorReference> {
    var result : List<VehicleRiskIndicatorReference> = null
    if (_riFactorsFillable.VehicleType == null) {
      _riFactorsFillable.removeLicensePlate()
    } else {
      result = this.preselectRiskIndicator(asOfDate)
      _riFactorsFillable.RiskIndicator = result.first()
      applyLicensePlateType(_riFactorsFillable.VehicleType, _riFactorsFillable.RiskIndicator, asOfDate)
    }
    _riFactorsFillable.refreshRegionalClassification()
    if(_riFactorsFillable typeis Coverable) {
      NoClaimDiscounts.syncs(_riFactorsFillable)
    }
    return result
  }

  function applyLicensePlateType(vehicleType: VehicleTypeReference, riskIndicator: VehicleRiskIndicatorReference, asOfDate: Date) {
    var licensePlate = vehicleType.getSuggestedLicensePlateType(asOfDate, riskIndicator)
    _riFactorsFillable.applyLicensePlateType(licensePlate)
  }

  function refreshLicensePlateTypes(asOfDate: Date): LicensePlateReference[] {
    if(_riFactorsFillable.RiskIndicator != null) {
      return _riFactorsFillable.VehicleType.getAvailableLicensePlateTypesForRiskIndicator(asOfDate, _riFactorsFillable.RiskIndicator)
    }
    return getLicensePlateTypesforNoRiskIndicator(asOfDate)
  }

  function preselectVehicleType(asOfDate : Date){
    var validVehicleTypes = VehicleTypeReference.finder.findAll(asOfDate)

    var validVehicleTypesForRiskIndicator = getPossibleVehicleTypesForRiskIndicator(_riFactorsFillable.RiskIndicator)
    var result = validVehicleTypes.where(\elt -> validVehicleTypesForRiskIndicator.contains(elt))

    if(result.Count == 1) {
      _riFactorsFillable.VehicleType = result.first()
      this.onVehicleTypeChange(asOfDate)
    }
  }

  function onRiskIndicatorChange(asOfDate : Date){
    _riFactorsFillable.removeLicensePlate()

    if(_riFactorsFillable.RiskIndicator != null) {
      _riFactorsFillable.refreshRegionalClassification()
      this.preselectVehicleType(asOfDate)
      _riFactorsFillable.VehicleUsageCode = null
    }else{
      _riFactorsFillable.VehicleUsageCode = VehicleUsage.TC_O.Code
    }
  }

  function onRiskIndicatorPick(pickedValue : RiskIndicatorSearchResultDTO){
    _riFactorsFillable.RiskIndicator = pickedValue.RiskIndicator;
    _riFactorsFillable.VehicleType = pickedValue.VehicleType;
    _riFactorsFillable.applyLicensePlateType(pickedValue.LicensePlateType);
    _riFactorsFillable.refreshRegionalClassification()
  }

  private function getPossibleVehicleTypesForRiskIndicator(riskIndicator : VehicleRiskIndicatorReference) : List<VehicleTypeReference> {
    return RiskIndicatorFinder.getPossibleVehicleTypesForRiskIndicator(riskIndicator)
  }

  private function getPossibleRiskIndicatorsForVehicleType(vehicleType : VehicleTypeReference) : List<VehicleRiskIndicatorReference> {
    return RiskIndicatorFinder.getPossibleRiskIndicatorForVehicleType(vehicleType)
  }

  /**
   * Returns all license plate types that are relevant for the eVB process to enable requesting eVB numbers without a risk indicator
   * chose previously
   */
  @Param("asOfDate", "The date to apply as a restriction criteria.")
  @Returns("License plate type which are in effect as of the input date.")
  function getLicensePlateTypesforNoRiskIndicator(asOfDate : Date) : LicensePlateReference[] {
    return Query.make(LicensePlateReference)
        .compare(LicensePlateReference#eVBRelevant, Equals, true)
        .select()
        .orderByDescending(QuerySelectColumns.path(Paths.make(LicensePlateReference#Priority)))
        .toTypedArray()
  }

  function onVehicleTypeChange(asOfDate : Date, value : VehicleTypeReference) : List<VehicleRiskIndicatorReference> {
    _riFactorsFillable.VehicleType = value
    return onVehicleTypeChange(asOfDate)
  }
}
