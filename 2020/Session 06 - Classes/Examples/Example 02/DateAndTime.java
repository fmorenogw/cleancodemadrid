public class DateAndTime {
	
    public String timeZone = "UTC";
    private int seconds;
    private int minutes;
    private int hours;
    private int day;
    private int month;
    private int year;

	//GETTERS AND SETTERS
	
    public String getTime() {
        return "Time: " +
                "seconds=" + seconds +
                ", minutes=" + minutes +
                ", hours=" + hours +
                ", timeZone='" + timeZone + '\'' +
                '}';
    }

    public String getDate() {
        return "Date: " +
                "day=" + day +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
