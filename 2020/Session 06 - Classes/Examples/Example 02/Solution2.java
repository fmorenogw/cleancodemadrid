//public class Time
public class Solution2 {
    private int seconds;
    private int minutes;
    private int hours;
    private String timeZone = "UTC";

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTime() {
        return "Time: " +
                "seconds=" + seconds +
                ", minutes=" + minutes +
                ", hours=" + hours +
                ", timeZone='" + timeZone + '\'' +
                '}';
    }
}
