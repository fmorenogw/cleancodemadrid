class Jet {
  private var _engine : JetEngine

  function takeOff() {
    if (_engine.startEngine() != JetEngine.ENGINE_FAILURE) {
      try {
        _engine.accelerate()
      } catch (e : MechanicalError) {
        //stops acceleration process and shows error message
        logger.log(e)
      } catch (e : AccelerationError) {
        //stops acceleration process and shows error message
        logger.log(e)
      }
    } else {
      logger.log("Engine Failure: Unable to start. Takeoff aborted")
    }
  }
}

class JetEngine {
  static final var ENGINE_FAILURE : String = "ENGINE FAILURE"

  function startEngine() : String {
    var engineCanStart = true //mock variable
    // Starts Engine
    //...

    if(engineCanStart){
      return null
    } else {
      return ENGINE_FAILURE
    }
  }

  function accelerate() {
    var mechanicalError = false     //mock variable
    var engineCanAccelerate = true  //mock variable
    // Accelerates
    //...

    if(mechanicalError) {
      throw new MechanicalError("Error detected")
    }
    if(not engineCanAccelerate) {
      throw new AccelerationError("Error detected")
    }
  }
}