public class GiatApi {
    private String assessmentId;
    private AssuranceHelperApi api;

    public GiatApi(String assessmentId) {
        this.assessmentId = assessmentId;

        this.api = new ApiClient().buildClient(AssuranceHelperApi.class);
    }

    public void getProblems() {

        File inspectionsDir = Project.getInstance().getInspectionDirectory();
        if (!inspectionsDir.isDirectory()) {
            Project.getInstance().getVisitPanel().printToTextPane(inspectionsDir.getAbsolutePath() + " " + DisplayKey.get("not.a.directory"));
            throw new IllegalArgumentException(inspectionsDir.getAbsolutePath() + " " + DisplayKey.get("not.a.directory"));
        }

        EnrichmentHelper enrichmentHelper = EnrichmentHelper.getInstance();
        File[] fileList = inspectionsDir.listFiles(new XmlFileNameFilter());

        QuestionUpdate questionUpdate = new QuestionUpdate();

        for (File f : fileList) {
            try {
                Problems p = enrichmentHelper.enrichAndSaveIssues(f, false);
                if (p.getProblem().size() > 0) {
                    Problems.Problem prob = p.getProblem().get(0);
                    Question question = new Question();
                    String questionId = prob.getQuestionId();
                    if (questionId != null) {
                        question.setQuestionID(questionId);
                    }
                    String findingTitle = prob.getProblemClass().getValue(); // finding title
                    question.setFindingTitle(findingTitle);
                    String findingDetails = prob.getDescription();  // finding details
                    question.setFindingDetails(findingDetails);

                    questionUpdate.addQuestionsItem(question);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace(); // remove
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        try {
            api.updateAssessment(assessmentId, questionUpdate);
            Project.getInstance().printToTextPane("The data has been processed correctly by the GIAT");
        } catch (FeignException.FeignClientException.NotFound e) {
            Project.getInstance().printToTextPane("GIAT INTEGRATION ERROR: 404 Not Found");
        } catch (FeignException.FeignClientException.BadRequest e) {
            Project.getInstance().printToTextPane("GIAT INTEGRATION ERROR: 400 Bad Request");
        }
    }
}