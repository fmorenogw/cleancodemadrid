class Jet2 {
  private var _engine : JetEngine2

  function takeOff() {
    try {
      _engine.startEngine()
      _engine.accelerate()
    } catch (e : AccelerationCommonError) {
      //AccelerationCommonError is a parent for MechanicalError and AccelerationError
      logger.log("Jet2#takeOff () - Takeoff aborted. " + e)
      stopAccelerationProcess()
    } catch (e : RuntimeException) {
      logger.log("Jet2#takeOff () - Takeoff aborted. " + e)
    }
  }

  function stopAccelerationProcess() {
    //stops acceleration process and shows error message
  }
}

class JetEngine2 {
  static final var ENGINE_FAILURE : String = "ENGINE FAILURE"

  function startEngine() : String {
    var engineCanStart = true //mock variable
    // Starts Engine
    //...

    if(not engineCanStart){
      throw new RuntimeException("Engine unable to start")
    }
  }

  function accelerate() {
    var mechanicalError = false     //mock variable
    var engineCanAccelerate = true  //mock variable
    // Accelerates
    //...

    if(mechanicalError) {
      throw new MechanicalError("Mechanical Error detected during acceleration")
    }
    if(not engineCanAccelerate) {
      throw new AccelerationError("Engine cannot accelerate")
    }
  }
}