class Employee {
  var _id : int as ID
  var _daysInCompany : int as DaysInCompany
  var _age : int as Age
  var _skillsAcquired : int as SkillsAcquired

  construct(id : int, daysInCompany : int, age : int, skillsAcquired : int){
    _id = id
    _daysInCompany = daysInCompany
    _age = age
    _skillsAcquired = skillsAcquired
  }
}

static class DataManagement {
  static function getEmployees() : Employee[] {
    var employees : Employee[]
    //Connect to DB and retrieve all Employees
    return employees
  }

  static function writeEmployees(employees : Employee[]) {
    //Connect to DB and write employees to DB
  }
}

class Company {
  private var _employees = DataManagement.getEmployees()

  function countEmployees() : int {
    _employees = DataManagement.getEmployees()
    return _employees.length
  }

  function addSkillsToEmployee(employeeID : int, skillsNumber : int) {
    var targetEmployee = _employees.firstWhere(\employee -> employee.ID == employeeID)
    targetEmployee.SkillsAcquired += skillsNumber
  }

  function addNewEmployee(employee : Employee) {
    var employees : Employee[]
    employees[0] = employee
    addNewEmployee(employee)
    DataManagement.writeEmployees(employees)
  }
}
