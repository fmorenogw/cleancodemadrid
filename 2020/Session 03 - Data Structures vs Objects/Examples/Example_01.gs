class Employee {
  private var _id : int
  private var _daysInCompany : int
  private var _age : int
  private var _skillsAcquired : int

  function getId() : int  {
    return _id
  }

  function getDaysIncompany() : int  {
    return _daysInCompany
  }

  function getAge() : int  {
    return _age
  }

  function getSkillsAcquired() : int  {
    return _skillsAcquired
  }

  function setId(value : int)  {
    _id = value
  }

  function setDaysIncompany(value : int) {
    _daysInCompany = value
  }

  function setAge(value : int) {
    _age = value
  }

  function setSkillsAcquired(value : int) {
    _skillsAcquired = value
  }

  function acquireNewSkills(numberOfSkills : int){
    _skillsAcquired += numberOfSkills
  }
}

static class DataManagement {
  static function getEmployees() : Employee[] {
    var employees : Employee[]
    //Connect to DB and retrieve all Employees
    return employees
  }
  static function writeEmployees(employees : Employee[]) {
    //Connect to DB and write employees to DB
  }
}

class Company {
  var _numberOfEmployees = countEmployees()

  private function countEmployees() : int {
    return DataManagement.getEmployees().length
  }

  function addSkillsToEmployee(employeeID : int, skillsNumber : int) {
    DataManagement.getEmployees().firstWhere(\employee -> employee.getId() == employeeID).acquireNewSkills(skillsNumber)
  }

  function addNewEmployee(employee : Employee) {
    var employees : Employee[]
    employees[0] = employee
    DataManagement.writeEmployees(employees)
  }
}