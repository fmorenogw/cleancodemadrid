// Differences between Procedural and Object-Oriented programming
// Procedural programming
interface List { }

class NumberList implements List {
  var _numbers : int[]

  /*
  Constructors and methods to retrieve data
   */
}

class OrderedNumberList implements List {
  var _numbers : int[]

  /*
  Constructors and methods to retrieve data
   */
}

class StringList implements List {
  var _strings : String[]

  /*
  Constructors and methods to retrieve data
   */
}

class ObjectList implements List {
  var _objects : Object[]
  
  /*
  Constructors and methods to retrieve data
   */

}

static class NumberManager {

  public function max(list : List){
    if (list typeis NumberList){
      // return max of NumberList
    } else if (list typeis OrderedNumberList) {
      // return max of OrderedNumberList
    } else if (list typeis StringList){
      // return max of StringList
    } else if (list typeis ObjectList){
      // return max of ObjectList
    ]
  }

  public function min(list : List){
    if (list typeis NumberList){
      // return min of NumberList
    } else if (list typeis OrderedNumberList) {
      // return min of OrderedNumberList
    } else if (list typeis StringList){
      // return min of StringList
    }
  }

  public function first(list : List){
    if (list typeis NumberList){
      // return first of NumberList
    } else if (list typeis OrderedNumberList) {
      // return first of OrderedNumberList
    } else if (list typeis StringList){
      // return first of StringList
    }
  }

  public function last(list : List){
    if (list typeis NumberList){
      // return last of NumberList
    } else if (list typeis OrderedNumberList) {
      // return last of OrderedNumberList
    } else if (list typeis StringList){
      // return last of StringList
    }
  }
}


// Object-oriented programming
interface Player {
  function run()
  function scoreGoal()
  function stop()
}

class Goalkeeper implements Player {
  override function run (){ }
  override function scoreGoal(){ }
  override function stop() { }
}

class Defender implements Player {
  override function run (){ }
  override function scoreGoal(){ }
  override function stop() { }
  
  function defend() {
  
  }
}

class Midfielder implements Player {
  override function run (){ }
  override function scoreGoal(){ }
  override function stop() { }
}

class Forward implements Player {
  override function run (){ }
  override function scoreGoal(){ }
  override function stop() { }
}

class Bench implements Player {
  override function run (){ }
  override function scoreGoal(){ }
  override function stop() { }
}