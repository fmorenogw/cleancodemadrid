public class BillableHoursTracker{

	private int billableHours;
	
	BillableHoursTracker(initialHours){
		
		this.billableHours = initialHours;
		
	}
	
	public int getBillableHours (){
		return billableHours;
	}
	
	public void addHours(int hours){
		// Implementation
	}
	
}

public class UnitTest2{
	
	public static int startingHours = 72;
	public static int increaseHours = 8;
	
	/*
	
	A lot of more tests
	
	*/
	
	public void testAddHours(){
		BillableHoursTracker hourTracker = new BillableHoursTracker(startingHours);
		hourTracker.addHours(increaseHours);
		Assert.IsTrue((startingHours + increaseHours) == hourTracker.getBillableHours());
	}
}