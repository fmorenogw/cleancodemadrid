package nmm.rules.assignment

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.databuilder.ClaimBuilder
uses gw.api.databuilder.PolicyBuilder
uses gw.pl.persistence.core.Bundle
uses gw.testharness.TestBase
uses gw.transaction.Transaction
uses nmm.databuilders.SampleAdminDataBuilderUtilTest

/**
 * Test class for the Activity Assignment Rules
 */
class ActivityAssignmentRulesTestSolution extends TestBase {
  private static var aClaim : Claim
  private static var aClaimBuilder : ClaimBuilder
  private static var anActivity : Activity

  override function beforeClass() {
    //Mock admin data needed to create the claim
    Transaction.runWithNewBundle(\bundle -> {
      SampleAdminDataBuilderUtilTest.createWCGroup(bundle)
    })

    //Claim creation
    Transaction.runWithNewBundle(\bundle -> {
      aClaimBuilder = new ClaimBuilder().withDefaults()
          .withClaimant(SampleAdminDataBuilderUtilTest.createClaimant())
          .withPolicy(new PolicyBuilder().withDefaults())
      aClaim = aClaimBuilder.create(bundle)
      aClaim.autoAssign()
    })
  }

  /**
   * NMM-GAA-Assign To Subro Group
   */
  function testNMMGAAAssignToSubroGroup() {
    Transaction.runWithNewBundle(\bundle -> {
      anActivity = aClaim.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("90_mgr_review_subro"))
      anActivity.autoAssign()
    })

    assertEquals("Activity has not been assigned correctly", GroupType.TC_SUBRO, anActivity.AssignedGroup.GroupType)
  }

  /**
   * NMM-GAA - Assign Contact Approval Activity
   */
  function testNMMGAAAssignToTaxIDApproversGroup() {
    var groupName = "Contact Approvers"
    var approversGroup = Query.make(Group).compare(Group#Name, Relop.Equals, groupName).select().AtMostOneRow

    Transaction.runWithNewBundle(\bundle -> {
      anActivity = aClaim.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("contact_approval_ext"))
      anActivity.autoAssign()
    })

    assertEquals("Activity has not been assigned correctly to the group", groupName, anActivity.AssignedGroup.Name)
    assertTrue("Activity has not been assigned correctly to the user", approversGroup.containsUser(anActivity.AssignedUser))
  }

  /**
   * NMM-DAA - Assign Reinsurance Activity
   */
  function testNMMDAAAssignReinsuranceActivity() {
    var reinsuranceManagerUserName = "colleenm"

    Transaction.runWithNewBundle(\bundle -> {
      anActivity = aClaim.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("claim_reinsurance_review"))
      anActivity.autoAssign()
    })

    assertEquals("Activity has not been assigned to the reinsurance manager.", reinsuranceManagerUserName, anActivity.AssignedUser.Credential.UserName)
  }
}