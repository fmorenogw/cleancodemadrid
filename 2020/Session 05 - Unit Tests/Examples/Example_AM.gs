package nmm.rules.assignment

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.databuilder.ClaimBuilder
uses gw.api.databuilder.PolicyBuilder
uses gw.testharness.TestBase
uses gw.transaction.Transaction
uses nmm.databuilders.SampleAdminDataBuilderUtilTest
uses entity.Activity

/**
 * Test class for the Activity Assignment Rules
 */
class ActivityAssignmentRulesTestIncorrect extends TestBase {

  private static var aClaim : Claim
  private static var aClaimBuilder : ClaimBuilder

  /**
   * NMM-GAA-Assign Activities Test
   */
  function testActivitiesAssignment() {
    var anActivity : Activity
    var anActivity2 : Activity
    var anActivity3 : Activity

    //Claim creation
    Transaction.runWithNewBundle(\bundle -> {
      SampleAdminDataBuilderUtilTest.createWCGroup(bundle)
    })

    Transaction.runWithNewBundle(\bundle -> {
      aClaimBuilder = new ClaimBuilder().withDefaults()
          .withClaimant(SampleAdminDataBuilderUtilTest.createClaimant())
          .withPolicy(new PolicyBuilder().withDefaults())
      aClaim = aClaimBuilder.withIncidentReport(true).create(bundle)
      aClaim.autoAssign()

      //Activity creation and assignment
      anActivity = aClaim.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("90_mgr_review_subro"))
      anActivity2 = aClaim.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("contact_approval_ext"))
      anActivity3 = aClaim.createActivityFromPattern(null, ActivityPattern.finder.getActivityPatternByCode("claim_reinsurance_review"))

      anActivity.autoAssign()
      anActivity2.autoAssign()
      anActivity3.autoAssign()
    })
    var approversGroup = Query.make(Group).compare(Group#Name, Relop.Equals, "Contact Approvers").select().AtMostOneRow

    assertEquals("Activity has not been assigned correctly", GroupType.TC_SUBRO, anActivity.AssignedGroup.GroupType)
    assertEquals("Activity 2 has not been assigned correctly to the group", "Contact Approvers", anActivity2.AssignedGroup.Name)
    assertTrue("Activity 2 has not been assigned correctly to the user", approversGroup.containsUser(anActivity2.AssignedUser))
    assertEquals("Activity 3 has not been assigned to the reinsurance manager.", "colleenm", anActivity3.AssignedUser.Credential.UserName)
  }
}